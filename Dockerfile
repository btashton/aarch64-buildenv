FROM debian:9
MAINTAINER bashton@brennanashton.com

RUN apt-get update && apt-get install -y wget xz-utils make gcc bc bison flex git && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /opt/linaro/aarch64
RUN cd /opt/linaro/aarch64 && wget --quiet --output-document linaro-linux-cross.tar.xz http://releases.linaro.org/components/toolchain/binaries/7.3-2018.05/aarch64-linux-gnu/gcc-linaro-7.3.1-2018.05-x86_64_aarch64-linux-gnu.tar.xz && tar xf linaro-linux-cross.tar.xz --strip-components=1 && rm linaro-linux-cross.tar.xz
RUN cd /opt/linaro/aarch64 && wget --quiet --output-document linaro-elf-cross.tar.xz http://releases.linaro.org/components/toolchain/binaries/7.3-2018.05/aarch64-elf/gcc-linaro-7.3.1-2018.05-x86_64_aarch64-elf.tar.xz && tar xf linaro-elf-cross.tar.xz --strip-components=1 && rm linaro-elf-cross.tar.xz
RUN cd /opt/linaro/aarch64/bin && \
    for i in aarch64-elf-*; do ln -s "$i" "aarch64-none-elf-${i#aarch64-elf-}"; done
env PATH /opt/linaro/aarch64/bin:$PATH
